<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//text
	$text = get_field('page_text');

	//bg
	$bg = get_field('page_img') ? : $bg = get_field('page_img', 'options');

	//product img
	$product = get_field('product_img');
?>

<section class="page__hero" style="background-image: url(<?php echo esc_url($bg['url']); ?>);">
	<div class="wrap hpad">
		<div class="row">
			<div class="col-sm-6 wow fadeInUp">
				<h1 class="page__title"><?php echo $title; ?></h1>
				<p><?php echo $text; ?></p>
			</div>	

			<?php if ($product) : ?>
			<div class="page__product col-sm-6 wow fadeInRight">
				<img class="page__product" src="<?php echo $product['url']; ?>" alt="<?php echo $product['alt']; ?>">
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>