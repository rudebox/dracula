<?php 
/**
* Description: Lionlab variants field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');
?>

<section class="variants black--bg padding--<?php echo esc_attr($margin); ?>">

	<div class="wrap hpad variants__container wow bounceIn">
		<div class="row flex flex--wrap flex--center">
			<?php  
				$title = get_sub_field('title');
				$img = get_sub_field('img');
				$maskot = get_sub_field('maskot');
				$text = get_sub_field('text');
				$link = get_sub_field('link');
			?>

			<div class="col-sm-4 variants__product-img">
				<?php if ($img) : ?>
				<img src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				<?php endif; ?>
			</div>

			<div class="variants__content col-sm-8 <?php echo esc_attr($bg); ?>--bg">
				<div class="row flex flex--wrap flex--center">
					<div class="col-sm-6 variants__item">
					
						<h2 class="variants__title"><?php echo esc_html($title); ?></h2>
						<p><?php echo esc_html($text); ?></p>

						<div class="variants__wrap flex flex--center">
							<?php if ($link) : ?>
							<a class="btn btn--red btn--red--flag" target="_blank" href="<?php echo esc_url($link); ?>"><span>Køb</span></a> 
							<?php endif; ?>
							<div class="fb-share-button variants__fb" data-href="<?php echo the_permalink(); ?>" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>" class="fb-xfbml-parse-ignore">Del</a></div>
						</div>
					</div>

					<div class="col-sm-6 variants__product-featured variants__product-featured--<?php echo esc_attr($bg); ?>">
						<?php if ($maskot) : ?>
						<img src="<?php echo esc_url($maskot['url']); ?>" alt="<?php echo esc_attr($maskot['alt']); ?>">
						<?php endif; ?>
					</div>
				</div>
				<div class="arrow"></div>
			</div>
			
		</div>
	</div>

</section>