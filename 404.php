<?php get_template_part('parts/header'); ?>

<main>
	
  <?php get_template_part('parts/page', 'header');?>

  <section class="error padding--both">
  	<div class="wrap hpad">
  		<p>Vi kunne desværre ikke finde den side du søgte efter.</p>
  	</div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>